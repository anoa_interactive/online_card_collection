﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using BLabTexasHoldEmProject;
using BLabProjectMultiplayer.LoginController;

namespace Anoa.Menu
{

    public class MainMenuManager : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {
            PhotonNetwork.PhotonServerSettings.JoinLobby = true;
        }

        public void ClickPlayGame()
        {
            StartCoroutine(GameManager.GetCountryCodeViaIP());
            //checkForMultiplayerRoomsScript.directConnectToBestServer = true;
            //checkForMultiplayerRoomsScript.useDirectAccess = false;
            //multiplayerConnectController.SetActive(true);
        }

        protected void ConnectMultiplayerGame()
        {
            //waitingForRealRoomsList = false;
            //canClick = false;

            //pingList = new int[10];
            //roomsList = new int[10];            

            PhotonNetwork.automaticallySyncScene = true;

            if (!PhotonNetwork.connected)
            {
                PhotonNetwork.PhotonServerSettings.HostType = ServerSettings.HostingOption.BestRegion;
                //PhotonNetwork.ConnectToBestCloudServer(BBStaticVariableMultiplayer.photonConnectionVersion);
                PhotonNetwork.ConnectUsingSettings(BBStaticVariableMultiplayer.photonConnectionVersion);
            }
            else
            {
                PhotonNetwork.Disconnect();
                GameManager.instance.ChangeScene(0);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}
