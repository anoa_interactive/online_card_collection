﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using BLabTexasHoldEmProject;
using BLabProjectMultiplayer.LoginController;

namespace Anoa
{

    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        public BBGlobalDefinitions BGlobalDefinitions;

        bool gotRegionChecked = false;
        string freeSeatRoomNane = "";
        bool gotFreeSeatOnRoom = false;

        void Awake()
        {
            instance = this;
        }

        // Use this for initialization
        IEnumerator Start()
        {
            Debug.Log("Start BGlobalDefinitions.UseLoginSystem : " + BGlobalDefinitions.UseLoginSystem +
            " BGlobalDefinitions.UseDirectFindASeatThenGoIn : " + BBStaticVariableMultiplayer.UseDirectFindASeatThenGoIn +
                  " connected : " + PhotonNetwork.connected);


#if UNITY_EDITOR
            gameObject.AddComponent<BLabTexasHoldEmProject.BBGetScreenShoot>();
#endif

#if UNITY_STANDALONE

   if(BGlobalDefinitions.UseLoginSystem == false) {
		WebCamDevice[] camList = WebCamTexture.devices;
         	
		if(camList.Length > 0) {           	      
			yield return Application.RequestUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone);
	        if (Application.HasUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone)) {
	                canUseWebCamera = true;
					webcamTexture = new WebCamTexture();
	                cam.texture = webcamTexture;
	                webcamTexture.Play();
	        } else {
	           canUseWebCamera = false;
	        }
	    } else {
		   canUseWebCamera = false;
	    }

	    if(!canUseWebCamera) {
				GameObject img = GameObject.Find("RawImageWebCam");
				if(img) img.SetActive(false);
				GameObject butt = GameObject.Find("ButtonGetCamPicture");
				if(butt) butt.SetActive(false);
	    }
   }
#endif

            PlayerPrefs.SetString("myAvatarImageName", PlayerPrefs.GetString(ProductionMainMenuController.PlayerPreferKeyNames.PLAYER_SHARE_PICTURE));
            BBStaticVariable.myAvatarImageNameIdx = "20";

            yield break;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public static IEnumerator GetCountryCodeViaIP()
        {
            string countryCode = "XX";
            string url = "http://www.geoplugin.net/json.gp";

            WWW www = new WWW(url);
            float startTime = Time.time;

            Debug.Log("GetCountryCodeViaIP : " + url);

            // Wait for download to complete
            while (!www.isDone)
            {
                if (www.error != null || Time.time - startTime > 8.0f) break;
                yield return new WaitForSeconds(0.2f);
            }

            if (www.error != null)
            {
                Debug.Log(www.error);
                countryCode = "XX";
                PlayerPrefs.SetString("countryCode", countryCode);
            }
            else
            {
                //Debug.Log(www.error); 

            }

            //Debug.Log(www.text); 

            if (www.isDone && www.error == null && www.text != null)
            {


                countryCode = www.text.Substring(www.text.IndexOf("countryCode") + 11 + 3, 2);
                //countryCode = www.text.Substring(www.text.IndexOf("countryCode") + 17, 2);

                Debug.Log("CountryCode from IP: " + countryCode);

                PlayerPrefs.SetString("countryCode", countryCode);
            }
            else
            {
                countryCode = "XX";
                PlayerPrefs.SetString("countryCode", countryCode);
            }
        }

        public void ChangeScene(int index)
        {
            SceneManager.LoadScene(index);
        }

        public void ChangeScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }
    }

}
